# Magnetic bearing measurement

Measure magnetic bearing and distance between two points.

* Toggle between geodetic (° T) and magnetic (° M) bearings.
* View declination and model uncertainty estimate at start point.
* Uses the [World Magnetic Model (WMM)](https://www.ngdc.noaa.gov/geomag/WMM/) in its [WMM2025](http://www.geomag.bgs.ac.uk/documents/WMM2020_Report.pdf "WMM2020 report (this is what is cited in the NOAA website)") realisation.
* [Temporal controller](https://qgis.org/pyqgis/master/core/QgsTemporalController.html) aware.

![](media/screenshot.png)

This plugin adds a new map tool to QGIS. It can be activated:

* from the [QGIS measurement toolbox](https://docs.qgis.org/latest/en/docs/user_manual/introduction/general_tools.html#measuring) (![](media/mActionMeasure.png));
* from the menu (View → Measure → Measure Magnetic Bearing);

### Reference epoch

If temporal navigation is enabled, the declination values correspond to the timestamp of the beginning of the current frame, provided that it falls within the range of the model (2025.0‒2030.0 as of this version). If the timestamp is outside this range the measurement will be aborted and a warning message shown.

In the following example, the declination value is that at 2021-12-15T14:43:01 (local time in the user's timezone in this case).

![](media/temporal-controller.png)

If temporal navigation is disabled, the reference epoch is current UTC time, according to the user's computer clock.

## The magnetic model

In order to avoid dependencies the tool uses a grid of sea level declination values obtained via the [`wmm_grid` software tool](https://www.ngdc.noaa.gov/geomag/WMM/soft.shtml) supplied with the model. The declination and uncertainty values are packed in a more compact binary form via the [`prepare-grid.py`](prepare-grid.py) script.

The grid has these characteristics:

Attribute           | Value
--------------------|----------------------------------
Geographic coverage | 180° W to 180° E; 90° S to 90° N
Geographic bin size | 1° × 1°
Temporal coverage   | 2025.0 to 2030.0
Temporal step       | 1 year
Altitude            | 0 m above mean sea level (EGM 96)

### Changing the grid

If the supplied grid does not satisfy your requirements, it can be replaced with a new one. To do this you will need a copy of the model's software tools.

Create the new grid by running `wmm_grid` with your chosen parameters. The output variable *must* be the declination value and only one altitude value is supported.

Assuming that you have saved the output of `wmm_grid` to the default file name, run:

```bash
./prepare-grid.py GridResults.txt wmmdata.bin
```

Replace the `wmmdata.bin` file supplied in this code with yours.

Finally, edit the `MagneticModel` class constructor in [`magnetic_model.py`](magnetic_model.py), replacing the following variables with the values appropriate for your grid:

Value    | Description
---------|-------------------------------------
`min_x`  | Minimum longitude (e.g., -180)
`min_y`  | Minimum latitude (e.g., -90)
`min_t`  | Minimum epoch (e.g., 2025.0)
`max_x`  | Maximum longitude (e.g., 180)
`max_y`  | Maximum latitude (e.g., 90)
`max_t`  | Maximum epoch (e.g., 2030.0)
`step_x` | Longitude step in degrees (e.g., 1)
`step_y` | Latitude step in degrees (e.g., 1)
`step_t` | Epoch step in years (e.g., 1)


## Development

Clone the plugin repository and run one of:

```bash
make compile
```

## Packaging a new version

```bash
make derase # This will remove the plugin from your QGIS profile.
make zip    # This will create a ZIP file and reinstall the plugin as a side effect.
```

## Credits

The rubber band implementation is based on code from https://github.com/webgeodatavore/azimuth_measurement ([GPL 2.0](https://github.com/webgeodatavore/azimuth_measurement/blob/2ab6db8ea11963bee9c666a77187e68635ff91d2/LICENSE))

The ![](icon.svg) icon is derived from [work](https://materialdesignicons.com/icon/angle-acute) by Haley Halcyon and [distributed](https://github.com/Templarian/MaterialDesign/issues/5777#issuecomment-902664083) under the [Apache 2.0](https://www.apache.org/licenses/LICENSE-2.0) licence.

This code itself is based upon the [Azimuth Measurement Map Tool](https://gitlab.com/aaltronav/foss/qgis/plugins/generic/azimuth-measurement).
