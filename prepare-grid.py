#!/usr/bin/env python3
#
# Script to create a data file for the magnetic bearing plugin.
#
# Must be used together with the World Magnetic Model software
# (https://www.ngdc.noaa.gov/geomag/WMM/soft.shtml)
#
# Run wmm_grid to produce a grid file (normally saved as GridResults.txt);
# this file may need cleaning up the warnings on the last few lines, then
# run:
#
# ./prepare_grid.py GridResults.txt wmmdata.bin
#
# Do not forget to edit the constructor in magnetic_model.py with the
# parameters matching your output grid (extents, resolution, etc.)
#

import pickle

def parse_gridfile(filename):

	values = []
	with open(filename, "r") as infile:
		while True:
			line = infile.readline()
			if not line:
				return values

			values +=  list(map(lambda v: round(float(v)*100), line.split()[4:]))

	return values


def write_values(filename, values):

	with open(filename, "wb") as outfile:
		protocol = 4
		pickle.dump(values, outfile, protocol)

if __name__ == '__main__':
	import sys

	infile = sys.argv[1]
	outfile = sys.argv[2]

	if infile and outfile:
		write_values(outfile, parse_gridfile(infile))

	exit(0)
