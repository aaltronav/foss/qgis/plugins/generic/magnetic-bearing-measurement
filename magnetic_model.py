
from os import path
import pickle
import math
from datetime import datetime, timezone

def from_decimal_year(year):
	"""Convert a decimal year to a UTC date"""

	base = int(year)
	fraction = year - base

	ts0 = datetime(base, 1, 1, tzinfo=timezone.utc)
	ts1 = datetime(base+1, 1, 1, tzinfo=timezone.utc)

	d1  = t1 - ts0

	return ts0 + fraction * d1

def to_decimal_year(ts):
	"""Convert from a UTC date to a decimal year"""

	base = ts.year

	ts0 = datetime(base, 1, 1, tzinfo=timezone.utc)
	ts1 = datetime(base+1, 1, 1, tzinfo=timezone.utc)

	d1 = ts1 - ts0

	return base+((ts-ts0)/d1)

def normalise_x(x):
	"""Normalise longitude to the range -180;180"""
	return ((x+180) % 360) - 180

def normalise_y(y):
	"""Normalise latitude to the range -90;90"""
	return ((y+90) % 360) - 90

def linear_interpolation(p0, p1, x):
	"""Interpolate a value for x
	where x is the normalised distance between p0 and p1"""

	return p0 + (x * (p1 - p0))

def bilinear_interpolation(p00, p01, p10, p11, x, y):
	"""Bilinear interpolation at point (x,y)"""

	return linear_interpolation(
		linear_interpolation(p00, p10, x),
		linear_interpolation(p01, p11, x),
		y)

def trilinear_interpolation(p000, p001, p010, p011, p100, p101, p110, p111, x, y, z):
	"""Trilinear interpolation at point (x,y,z)"""

	return linear_interpolation(
		bilinear_interpolation(p000, p001, p010, p011, x, y),
		bilinear_interpolation(p100, p101, p110, p111, x, y),
		z)

class MagneticModel():
	"""WMM data values"""

	def __init__(self):

		# Change these values whenever updating wmmdata.bin

		self.min_x = -180
		self.min_y = -90
		self.min_t = 2025.0

		self.max_x = 180
		self.max_y = 90
		self.max_t = 2030.0

		self.step_x = 1
		self.step_y = 1
		self.step_t = 1 # year


		# The values increment:
		# year, x, y (for single z)
		self.range_t = (self.max_t - self.min_t) / self.step_t + 1
		self.range_x = (self.max_x - self.min_x) / self.step_x + 1
		self.range_y = self.range_x*self.range_t

		# How many elements per datum?
		self.num_elements = 2

		# Load magnetic data
		datafilename = path.join(path.dirname(__file__), "wmmdata.bin")
		with open(datafilename, "rb") as infile:
			self.values = pickle.load(infile)

	def value_at(self, x, y, t = None, ts = None):
		"""Declination value at point (x, y) and epoch t

		:arg x:  Longitude in degrees
		:arg y:  Latitude in degrees
		:arg t:  Epoch as decimal year
		:arg ts: Epoch as UTC datetime object

		If both t and ts are provided, t takes priority.
		If none are provided, current computer time is used.

		Returns two values: declimation and uncertainty estimate.
		"""

		if t is None:
			if ts is None:
				ts = datetime.now(timezone.utc)

			t = to_decimal_year(ts)

		if t < self.min_t or t > self.max_t:
			raise ValueError("timestamp is out of range")

		# Normalise x and y
		x = normalise_x(x)
		y = normalise_y(y)

		if x < self.min_x or x > self.max_x:
			raise ValueError("x is out of range")

		if y < self.min_y or y > self.max_y:
			raise ValueError("y is out of range")

		t0 = math.floor((t-(self.min_t))/self.step_t)
		x0 = math.floor((x-(self.min_x))/self.step_x)
		y0 = math.floor((y-(self.min_y))/self.step_y)

		t1 = (math.floor((t-(self.min_t))/self.step_t)+1) if t != self.max_t else t0
		x1 = (math.floor((x-(self.min_x))/self.step_x)+1) % self.range_x
		y1 = (math.floor((y-(self.min_y))/self.step_y)+1) % self.range_y

		dt = (t-math.floor(t))/self.step_t
		dx = (x-math.floor(x))/self.step_x
		dy = (y-math.floor(y))/self.step_y

		# These are the indices into the data of eight values
		# needed for a cubic interpolation.

		# posyxt
		pos000 = int(y0*self.range_y+x0*self.range_t+t0) # y0 x0 t0
		pos001 = int(y0*self.range_y+x0*self.range_t+t1) # y0 x0 t1
		pos010 = int(y0*self.range_y+x1*self.range_t+t0) # y0 x1 t0
		pos011 = int(y0*self.range_y+x1*self.range_t+t1) # y0 x1 t1
		pos100 = int(y1*self.range_y+x0*self.range_t+t0) # y1 x0 t0
		pos101 = int(y1*self.range_y+x0*self.range_t+t1) # y1 x0 t1
		pos110 = int(y1*self.range_y+x1*self.range_t+t0) # y1 x1 t0
		pos111 = int(y1*self.range_y+x1*self.range_t+t1) # y1 x1 t1

		# Declination values
		val000 = self.values[pos000 * self.num_elements]
		val001 = self.values[pos001 * self.num_elements]
		val010 = self.values[pos010 * self.num_elements]
		val011 = self.values[pos011 * self.num_elements]
		val100 = self.values[pos100 * self.num_elements]
		val101 = self.values[pos101 * self.num_elements]
		val110 = self.values[pos110 * self.num_elements]
		val111 = self.values[pos111 * self.num_elements]

		# Uncertainty values
		err000 = self.values[pos000 * self.num_elements + 1]
		err001 = self.values[pos001 * self.num_elements + 1]
		err010 = self.values[pos010 * self.num_elements + 1]
		err011 = self.values[pos011 * self.num_elements + 1]
		err100 = self.values[pos100 * self.num_elements + 1]
		err101 = self.values[pos101 * self.num_elements + 1]
		err110 = self.values[pos110 * self.num_elements + 1]
		err111 = self.values[pos111 * self.num_elements + 1]

		value = trilinear_interpolation(
			val000, val100, val010, val110,
			val001, val101, val011, val111,
			dx, dy, dt)

		# We return the maximum uncertainty for any of the data points
		uncertainty = max(err000, err001, err010, err011, err100, err101, err110, err111)

		# Values in wmmdata.bin are scaled by 100 so we can store them
		# as integers.
		return value/100, uncertainty/100
